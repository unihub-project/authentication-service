/**
 * Created by bdrosatos on 25/9/2016.
 */
module.exports = {
    dbUrl: 'mongodb://admin:a199412@ds129010.mlab.com:29010/unihub-authentication',
    testDatabase: 'mongodb://uniexamstest:uniexams2016@ds057816.mlab.com:57816/uni-exams-test-db',
    secret: 'SUPERSECRETKEYFORUNIEXAMS',
    facebookAuth: {
        'clientID': '1760075784255280',
        'clientSecret': '16e45d385cb9c84f312f95c6911be5d5',
        'callbackURL': 'http://localhost:5035/api/users/authenticate/facebook/callback'
    },
    port: process.env.PORT || 5000,
    email_options: {
        auth: {
            api_user: 'johnmara',
            api_key: 'johnbill137'
        }
    }
};