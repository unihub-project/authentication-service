/**
 * Created by bdros on 12-Mar-17.
 */
module.exports = {
    "hydra": {
        "serviceName": "authentication-service",
        "serviceDescription": "",
        "servicePort": process.env.PORT || 5000,
        "redis": {
            "url": "172.17.0.2",
            "port": 6379,
            "db" : 15
        }
    }
};