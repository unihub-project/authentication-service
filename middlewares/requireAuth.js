/**
 * Created by bdrosatos on 25/2/2017.
 */
'use strict';

const passport = require('passport'),
    i18n = require("i18n");

require('../config/passport');


exports.requireAuth = (req, res, next) => {
    passport.authenticate('jwt', {session: false}, function (err, user) {
        if (err) {
            return next(err);
        }
        if (!user) {
            return res.status(401).json({
                title: i18n.__('WORD_UNAUTHORIZED'),
                error: {message: i18n.__('INVALID_TOKEN')}
            });
        }

        req.user = user;   // Forward user information to the next middleware
        next();
    })(req, res, next);
};