"use strict";

const hydraExpress = require('fwsp-hydra-express'),
    mongoose = require('mongoose'),
    swagger = require('swagger-express'),
    morgan = require('morgan'),
    cookieParser = require('cookie-parser'),
    RateLimit = require('express-rate-limit'),
    bodyParser = require('body-parser'),
    i18n = require("i18n"),
    logger = require('noogger'),
    appConfig = require('./config/app'),
    authanticationServiceConfig = require('./config/authentication-service'),
    version = require('./package').version;


hydraExpress.init(authanticationServiceConfig, version, registerRoutesCallback, registerMiddlewares)
    .then((serviceInfo) => {
        console.log('serviceInfo', serviceInfo);
    })
    .catch((err) => {
        console.log('err', err);
    });

let app = hydraExpress.getExpressApp();

function registerMiddlewares() {
    mongoose.Promise = require('bluebird');
    mongoose.connect(appConfig.dbUrl);

//i18 use for translations based on browser locale (english, greek)
    i18n.configure({
        locales: ['en', 'el'],
        directory: __dirname + '/locales',
        defaultLocale: 'el'
    });

//Logger initialization
    logger.init({
        consoleOutput: true,
        consoleOutputLevel: ['DEBUG', 'ERROR', 'WARNING'],

        dateTimeFormat: "DD-MM-YYYY HH:mm:ss.S",
        outputPath: "logs/",
        fileNameDateFormat: "DD-MM-YYYY",
        fileNamePrefix: "authentication-service"
    });

// app.enable('trust proxy'); // only if you're behind a reverse proxy (Heroku, Bluemix, AWS if you use an ELB, custom Nginx setup, etc)

    const apiLimiter = new RateLimit({
        windowMs: 5 * 60 * 1000, // 5 mins
        max: 300, // limit each IP to 300 api requests per 5 mins
        delayMs: 0, // disable delaying - full speed until the max limit is reached,
        handler: function (req, res) {
            return res.status(429).json(
                {
                    title: '',
                    error: {message: i18n.__("TOO_MANY_REQUESTS")}
                });
        }

    });

    app.disable('x-powered-by');

    app.use(swagger.init(app, {
        apiVersion: '1.0',
        swaggerVersion: '1.0',
        basePath: 'http://localhost:' + 5035,
        swaggerURL: '/swagger',
        swaggerJSON: '/api-docs.json',
        swaggerUI: './public/swagger/',
        apis: ['./controllers/auth.controller.js']
    }));

    app.use(i18n.init);

    app.use(morgan('dev'));
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({extended: false, limit: '50mb'}));
    app.use(cookieParser());

// //CORS setup
    app.use((req, res, next) => {
        res.setHeader('Access-Control-Allow-Origin', '*');
        res.setHeader('Access-Control-Allow-Headers', 'Origin, Accept, Accept-Version, Content-Length, Content-MD5, Content-Type, Date, X-Api-Version, X-Response-Time, X-PINGOTHER, X-CSRF-Token, Authorization');
        res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
        next();
    });


//  apply to all requests
    app.use('/api/', apiLimiter);
}


function registerRoutesCallback() {
    hydraExpress.registerRoutes({
        '/api/users' : require('./routes/user.route'),
        '/api/profiles' : require('./routes/profile.route'),
    });

    // catch 404 and forward to error handler
    app.use((req, res, next) => {
        var err = new Error('Not Found');
        err.status = 404;
        next(err);
    });

// error handlers

// development error handler
// will print stacktrace
    if (app.get('env') === 'development') {
        app.use((err, req, res) => {
            res.status(err.status || 500);
            res.json({
                title: err,
                error: {message: err.message}
            });
        });
    }

// production error handler
// no stacktraces leaked to user
    app.use((err, req, res) => {
        res.status(err.status || 500);
    });

}