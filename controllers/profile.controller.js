/**
 * Created by bdros on 9/28/2016.
 */

"use strict";


const i18n = require('i18n'),
    Profile = require('../models/profile'),
    // Course = require('../models/course'),
    paginate = require('express-paginate'),
    logger = require('noogger');

//Private methods


//


//========================================
// Create Profile Route
//========================================

exports.createProfile = (req, res, next) => {
    const firstName = req.body.firstName;
    const lastName = req.body.lastName;
    const defaultDepartmentId = req.body.defaultDepartment;
    const uniRegistrationYear = req.body.uniRegistrationYear;
    const birthDate = req.body.birthDate;

    const user = req.user; //User to create profile

    //Check for errors
    if (!firstName) {
        return res.status(422).json({
            title: i18n.__('PROFILE_CREATE_ERROR_TITLE'),
            error: {message: i18n.__('PROFILE_CREATE_ERROR_FIRSTNAME')}
        });
    }

    if (!user) {
        return res.status(404).json({
            title: i18n.__('PROFILE_CREATE_ERROR_TITLE'),
            error: {message: i18n.__('NO_USER_FOUND')}
        });
    }

    //Has profile
    if (user.profile) {
        return res.status(422).json({
            title: i18n.__('PROFILE_CREATE_ERROR_TITLE'),
            error: {message: i18n.__('PROFILE_CREATE_ERROR_ALREADY')}
        });
    }

    const profile = new Profile({
        firstName: firstName,
        lastName: lastName,
        user: user._id,
        defaultDepartment: defaultDepartmentId,
        uniRegistrationYear: uniRegistrationYear,
        birthDate: birthDate
    });

    profile.save((err, newProfile) => {
        if (err) {
            return next(err);
        }

        user.profile = newProfile._id;

        user.save((err) => {

            if (err) {
                return next(err);
            }

            res.status(201).json({
                title: i18n.__('PROFILE_CREATE_SUCCESS_TITLE'),
                obj: newProfile
            });
        });

    });
};

//========================================
// Get List Profiles Route
//========================================

exports.getProfileList = (req, res, next) => {

    Profile.paginate({}, {
        page: req.query.page,
        populate: {
            path: 'user',
            select: '_id email role isConfirmed createdAt updatedAt'
        }
    }, (err, result) => {
        if (err) {
            return next(err);
        }


        const profiles = result.docs;

        res.status(200).json({
            has_more: paginate.hasNextPages(req)(result.pages),
            total_pages: result.pages,
            obj: profiles
        });

    });
};


//========================================
// Get Profile by id
//========================================

exports.getProfileById = (req, res, next) => {

    Profile.findById(req.params.id)
        .populate({
            path: 'user',
            select: '_id email role isConfirmed createdAt updatedAt'
        })
        // .populate({
        //     path: 'defaultDepartment',
        //     select: '_id name university',
        //     populate: {
        //         path: 'university',
        //         model: 'University',
        //         select: '_id name'
        //     }
        // })

        .exec((err, profile) => {
            if (err) {
                return next(err);
            }

            if (!profile) {
                return res.status(404).json({
                    title: i18n.__('PROFILE_GET_ERROR_TITLE'),
                    error: {message: i18n.__('PROFILE_NOT_FOUND')}
                });
            }

            res.status(200).json({
                title: i18n.__('PROFILE_GET_SUCCESS_TITLE'),
                obj: profile
            });
        });

};

//========================================
// Update Profile by id
//========================================

exports.updateProfile = (req, res, next) => {

    const firstName = req.body.firstName;
    const lastName = req.body.lastName;
    const birthDate = req.body.birthDate;
    const uniRegistrationYear = req.body.uniRegistrationYear;
    const defaultDepartment = req.body.defaultDepartment;

    Profile.findById(req.params.id)
        .populate({
            path: 'user',
            select: '_id email role isConfirmed createdAt updatedAt'
        })
        .exec((err, profile) => {
            if (err) {
                return next(err);
            }

            if (!profile) {
                return res.status(404).json({
                    title: i18n.__('PROFILE_GET_ERROR_TITLE'),
                    error: {message: i18n.__('PROFILE_NOT_FOUND')}
                });
            }

            if (!firstName) {
                return res.status(422).json({
                    title: i18n.__('PROFILE_UPDATE_ERROR_TITLE'),
                    error: {message: i18n.__('PROFILE_UPDATE_ERROR_FIRSTNAME')}
                });
            }

            profile.firstName = firstName;
            profile.lastName = lastName;
            profile.birthDate = birthDate;
            profile.uniRegistrationYear = uniRegistrationYear;
            profile.defaultDepartment = defaultDepartment;

            profile.save((err, updatedProfile) => {
                if (err) {
                    next(err);
                }

                res.status(200).json({
                    title: i18n.__('PROFILE_UPDATE_TITLE'),
                    obj: updatedProfile
                });
            });
        });

};

// //========================================
// // Follow Course by Id
// //========================================
//
// exports.followCourse = (req, res, next) => {
//     const courseId = req.params.id;
//     const profile = req.user.profile;
//
//     Course.findById(courseId).exec()
//         .then((course) => {
//
//             if (!course) {
//                 return res.status(404).json({
//                     title: i18n.__('PROFILE_FOLLOW_COURSE_ERROR_TITLE'),
//                     error: {message: i18n.__('COURSE_NOT_FOUND')}
//                 });
//             }
//
//             return course;
//
//         })
//
//         .then((course) => {
//             Profile.findById(profile).exec()
//                 .then((profile) => {
//
//                     if (profile.followCourses.find(c => c.equals(courseId))) {
//                         return res.status(422).json({
//                             title: i18n.__('PROFILE_FOLLOW_COURSE_ERROR_TITLE'),
//                             error: {message: i18n.__('COURSE_ALREADY_FOLLOW')}
//                         });
//                     }
//
//                     profile.followCourses.push(course._id);
//
//                     profile.save((err) => {
//
//                         if(err) {
//                             logger.critical(JSON.stringify(err));
//                             return next(err);
//                         }
//
//                         return res.status(200).json({
//                             title: i18n.__('PROFILE_FOLLOW_COURSE_SUCCESS_TITLE'),
//                             obj: profile
//                         });
//                     });
//                 });
//         })
//
//         .catch((err) => {
//             logger.critical(JSON.stringify(err));
//         });
//
//
// };
//
// exports.unFollowCourse = (req, res, next) => {
//     const courseId = req.params.id;
//     const profile = req.user.profile;
//
//     Course.findById(courseId).exec()
//         .then((course) => {
//
//             if (!course) {
//                 return res.status(404).json({
//                     title: i18n.__('PROFILE_UNFOLLOW_COURSE_ERROR_TITLE'),
//                     error: {message: i18n.__('COURSE_NOT_FOUND')}
//                 });
//             }
//
//             return course;
//
//         })
//
//         .then((course) => {
//             Profile.findById(profile).exec()
//                 .then((profile) => {
//
//                     if (!profile.followCourses.find(c => c.equals(courseId))) {
//                         return res.status(422).json({
//                             title: i18n.__('PROFILE_UNFOLLOW_COURSE_ERROR_TITLE'),
//                             error: {message: i18n.__('COURSE_NOT_FOLLOWED')}
//                         });
//                     }
//
//                     profile.followCourses.splice(profile.followCourses.indexOf(course._id), 1);
//
//                     profile.save((err) => {
//
//                         if(err) {
//                             logger.critical(JSON.stringify(err));
//                             next(err);
//                         }
//
//                         return res.status(200).json({
//                             title: i18n.__('PROFILE_UNFOLLOW_COURSE_SUCCESS_TITLE'),
//                             obj: profile
//                         });
//                     });
//                 });
//         })
//         .catch((err) => {
//             logger.critical(JSON.stringify(err));
//         });
//
//
// };



