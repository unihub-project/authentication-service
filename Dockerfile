FROM node:boron

# Create app directory
RUN mkdir -p /usr/src/unihub/authentication-service
WORKDIR /usr/src/unihub/authentication-service

# Install app dependencies
COPY package.json /usr/src/unihub/authentication-service
RUN npm install pm2 -g
RUN npm install

# Bundle app source
COPY . /usr/src/unihub/authentication-service

EXPOSE 5000
CMD [ "pm2-docker", "authentication-service.js" ]